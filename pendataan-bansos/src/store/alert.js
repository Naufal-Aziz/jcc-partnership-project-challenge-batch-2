export default {
    namespaced : true,
    state: {
        status: false,
        color: '#212121',
        text: ''
    },
    mutations: {
        SET_ALERT : (state, payload) => {
            state.status = payload.status
            state.color = payload.color
            state.text = payload.text
        }
    },
    actions: {
        set_alert :  ({commit}, payload) => {
            commit('SET_ALERT', payload)
        }
    },
    getters: {
        status: state => state.status,
        color: state => state.color,
        text: state => state.text,
    }
}