import Vue from 'vue'
import Vuex from 'vuex'
import alert from './alert'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: {
      nama: '',
      nik: '',
      kk: '',
      foto_ktp: null,
      foto_kk: null,
      umur: '',
      jenis_kelamin: '',
      alamat: '',
      rt: '',
      rw: '',
      pra_pandemi: '',
      pasca_pandemi: '',
      alasan: '',
    }
  },
  getters: {
  },
  mutations: {
    SET_NAMA(state, value) {
      state.data.nama = value
    },
    SET_NIK(state, value) {
      state.data.nik = value
    },
    SET_KK(state, value) {
      state.data.kk = value
    },
    SET_FOTO_KTP(state, value) {
      state.data.foto_ktp = value
    },
    SET_FOTO_KK(state, value) {
      state.data.foto_kk = value
    },
    SET_UMUR(state, value) {
      state.data.umur = value
    },
    SET_JENIS_KELAMIN(state, value) {
      state.data.jenis_kelamin = value
    },
    SET_ALAMAT(state, value) {
      state.data.alamat = value
    },
    SET_RT(state, value) {
      state.data.rt = value
    },
    SET_RW(state, value) {
      state.data.rw = value
    },
    SET_PRA_PANDEMI(state, value) {
      state.data.pra_pandemi = value
    },
    SET_PASCA_PANDEMI(state, value) {
      state.data.pasca_pandemi = value
    },
    SET_ALASAN(state, value) {
      state.data.alasan = value
    },
    CLEAR_FORM(state) {
      state.data.nama = ''
      state.data.nik = ''
      state.data.kk = ''
      state.data.foto_ktp = null
      state.data.foto_kk = null
      state.data.umur = ''
      state.data.jenis_kelamin = ''
      state.data.alamat = ''
      state.data.rt = ''
      state.data.rw = ''
      state.data.pra_pandemi = ''
      state.data.pasca_pandemi = ''
      state.data.alasan = ''
    },
  },
  actions: {
    set_nama({ commit }, value) {
      commit('SET_NAMA', value)
    },
    set_nik({ commit }, value) {
      commit('SET_NIK', value)
    },
    set_kk({ commit }, value) {
      commit('SET_KK', value)
    },
    set_foto_ktp({ commit }, value) {
      commit('SET_FOTO_KTP', value)
    },
    set_foto_kk({ commit }, value) {
      commit('SET_FOTO_KK', value)
    },
    set_umur({ commit }, value) {
      commit('SET_UMUR', value)
    },
    set_jenis_kelamin({ commit }, value) {
      commit('SET_JENIS_KELAMIN', value)
    },
    set_alamat({ commit }, value) {
      commit('SET_ALAMAT', value)
    },
    set_rt({ commit }, value) {
      commit('SET_RT', value)
    },
    set_rw({ commit }, value) {
      commit('SET_RW', value)
    },
    set_pra_pandemi({ commit }, value) {
      commit('SET_PRA_PANDEMI', value)
    },
    set_pasca_pandemi({ commit }, value) {
      commit('SET_PASCA_PANDEMI', value)
    },
    set_alasan({ commit }, value) {
      commit('SET_ALASAN', value)
    },
    clear_form({ commit }) {
      commit('CLEAR_FORM')
    },
  },
  modules: {
    alert
  }
})
