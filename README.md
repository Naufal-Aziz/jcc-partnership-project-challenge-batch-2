# JCC Partnership - Project Challenge Batch 2

Project #3 Frontend JS Project - Pendataan Bansos

[link directory app](https://gitlab.com/Naufal-Aziz/jcc-partnership-project-challenge-batch-2/-/tree/main/pendataan-bansos)

Dibuat oleh:

**Muhammad Naufal Al Aziz**

Kelas **Frontend Vue JS** | Candradimuka Jabar Coding Camp Batch 2

### Link deployment : [Netlify App](https://pendataan-bansos-naufal.netlify.app/)

### Link video demo : [Youtube](https://youtu.be/kj0VwjA6mZE)

---
## Mengenai Layout & Design

Dengan pertimbangan bahwa aplikasi akan dan hanya akan diakses melalui smartphone, maka design/layout terfokus pada tampilan *mobile web*.

*User* yang akan menggunakan aplikasi adalah pejabat tingkat RW dengan rentang usia 40 sampai dengan 50 tahun lebih, oleh karena itu desain sangatlah memperhatikan *user accessibility*. Mulai dari pemilihan warna yang memiliki kontras baik dan juga sesuai dengan standar dari **Jabar Design System**. Navigasi dengan jalan pintas yang memudahkan pengguna untuk dapat mengakses seluruh halaman yang tersedia di *web app* tanpa perlu melalui *navigation drawer*. Kemudian penggunaan *typeface* dengan ukuran yang nyaman bagi pengguna lanjut usia.

Ditambah lagi dengan *layout* formulir yang *straightforward*, dengan satu input pada setiap barisnya akan memudahkan pengguna untuk melacak ditahap mana proses pengisian data sedang berlangsung. Juga lengkap dengan indikator *focus* dan *error* menggunakan kode warna (*color coded*).

Terdapat juga pesan bantuan pada setiap errornya, juga beberapa toleransi serta alat bantu yang meminimalisir adanya kesalahan dalam proses pengisian formulir oleh pengguna.

Bahasa yang digunakan di dalam aplikasi secara umum adalah bahasa yang singkat namun dapat dengan mudah dimengerti oleh pengguna. Dengan demikian diharapkan struktur data yang diinput akan seragam dan sesuai satu dengan yang lainnya.

---

## Kontributor

- [M Naufal Al Aziz](https://gitlab.com/Naufal-Aziz)

- [Yohang88](https://gitlab.com/yohang88)

---
## Tentang Aplikasi


Aplikasi web ini dibuat sebagai pengajuan tugas
**JCC Partnership - Project Challenge Batch 2.**

Ditujukan untuk membantu pejabat tingkat RW dalam kebutuhan proses pendataan bansos di Komplek Panghegar, Kelurahan Cipadung Kulon, Kecamatan Panyileukan.

Aplikasi ini dibuat dengan mengadopsi standar design dari [**Jabar Design System**](https://digitalservice.jabarprov.go.id/design-system/).



